module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
    jest: true,
  },
  extends: ['plugin:react/recommended', 'airbnb', 'prettier'],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2022,
    sourceType: 'module',
  },
  plugins: ['react'],
  settings: {
    'import/resolver': {
      alias: {
        extensions: ['.js', '.jsx'],
      },
    },
  },
  rules: {
    'import/prefer-default-export': 0,
    'no-console': 0,
    'no-else-return': 0,
  },
};
