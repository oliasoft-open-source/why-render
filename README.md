# Why render?

React helper functions for debugging:

- Why components re-render
- Component rendering time

## Installation

```
yarn add --dev @oliasoft-open-source/why-render
```

**Note: this package is for debugging/testing only, and should not be merged into production code**

## Usage

### Explaining the differences between two arguments

There is a general-purpose function `explainDifferences` which compares (diffs) two arguments and explains why they
are different.

```
import { explainDiffernces } from '@oliasoft-open-source/why-render';

explainDifferences(previous, current);
```

### React

To use for class components:

```
import { whyRender } from '@oliasoft-open-source/why-render';

componentDidUpdate(prevProps, prevState) {
  whyRender(prevProps, this.props, prevState, this.state);
}
```

To use for function components:

```
import { useWhyRender } from '@oliasoft-open-source/why-render';

const Component = (props) => {
    useWhyRender(props);
}
```

```
import { useRenderTime } from '@oliasoft-open-source/why-render';

const Component = (props) => {
    useRenderTime();
}
```

There are also higher order components (HOCs) which can be used to wrap components with debug info:

- `withWhyRender`
- `withRenderTime`
- `withWhyRenderAndTime`
