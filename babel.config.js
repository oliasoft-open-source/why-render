module.exports = (api) => {
  api.cache(true);

  const presets = [
    [
      '@babel/preset-env',
      {
        targets: {
          browsers: ['defaults'],
        },
        useBuiltIns: 'usage',
        corejs: 3,
      },
    ],
  ];

  return {
    presets,
  };
};
