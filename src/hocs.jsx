// eslint-disable-next-line max-classes-per-file
import React from 'react';
import { whyRender } from './why-render';
import { measureRenderTime } from './render-time';

export const withWhyRender = (WrappedComponent) =>
  class WhyRenderHOC extends React.Component {
    componentDidUpdate(prevProps, prevState) {
      whyRender(prevProps, this.props, prevState, this.state);
    }

    render() {
      // eslint-disable-next-line react/jsx-props-no-spreading
      return <WrappedComponent {...this.props} />;
    }
  };

export const withRenderTime = (WrappedComponent) =>
  class WhyRenderHOC extends React.Component {
    componentDidMount() {
      this.startRenderTime = performance.now();
    }

    componentDidUpdate() {
      measureRenderTime(this);
    }

    render() {
      // eslint-disable-next-line react/jsx-props-no-spreading
      return <WrappedComponent {...this.props} />;
    }
  };

export const withWhyRenderAndTime = (WrappedComponent) =>
  class WhyRenderHOC extends React.Component {
    componentDidMount() {
      this.startRenderTime = performance.now();
    }

    componentDidUpdate(prevProps, prevState) {
      whyRender(prevProps, this.props, prevState, this.state);
      measureRenderTime(this);
    }

    render() {
      // eslint-disable-next-line react/jsx-props-no-spreading
      return <WrappedComponent {...this.props} />;
    }
  };
