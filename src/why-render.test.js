import { replacer, whyRender } from './why-render';

const consoleLogSpy = jest.spyOn(console, 'log');

describe('Why does a component render?', () => {
  describe('replacer replaces unsafe properties for JSON.stringify', () => {
    test('handles simple types', () => {
      const input = {
        a: 1,
        b: NaN,
        c: Infinity,
        d: -Infinity,
        e: () => {},
      };
      const result = JSON.parse(JSON.stringify(input, replacer));
      expect(result).toStrictEqual({
        a: 1,
        b: 'NaN',
        c: 'Infinity',
        d: '-Infinity',
        e: 'function',
      });
    });
    test('handles deep nesting', () => {
      const input = {
        a: {
          b: {
            c: 1,
            d: 'hello',
            e: NaN,
          },
          f: [2, NaN, Infinity],
          g: {
            h: () => {},
          },
        },
        i() {
          console.log('outer function');
        },
      };
      const result = JSON.parse(JSON.stringify(input, replacer));
      expect(result).toStrictEqual({
        a: {
          b: {
            c: 1,
            d: 'hello',
            e: 'NaN',
          },
          f: [2, 'NaN', 'Infinity'],
          g: {
            h: 'function',
          },
        },
        i: 'function',
      });
    });
  });
  describe('whyRender()', () => {
    beforeEach(() => {
      jest.resetAllMocks();
    });
    it('does not log when there are no changes', () => {
      const previousProps = { foo: 'bar' };
      const currentProps = previousProps;
      whyRender(previousProps, currentProps);
      expect(consoleLogSpy).not.toHaveBeenCalled();
    });
    it('logs details when props have changed', () => {
      const previousProps = { foo: 'bar' };
      const currentProps = { ...previousProps, baz: 'qux' };
      whyRender(previousProps, currentProps);
      expect(consoleLogSpy).toHaveBeenCalledTimes(2);
      expect(consoleLogSpy).toHaveBeenCalledWith(
        expect.stringContaining('Prop baz changed'),
        expect.stringContaining('color: #04D4F0'),
      );
      expect(consoleLogSpy).toHaveBeenCalledWith(
        expect.stringContaining('qux'),
      );
    });
  });
});
