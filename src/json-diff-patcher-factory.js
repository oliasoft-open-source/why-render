import { DiffPatcher } from 'jsondiffpatch';

/**
 * Creates new json diff patcher with default configuration.
 * DiffPatcher is used to figure out differences between objects/arrays,
 * to patch or unpatch objects/arrays, etc..
 */
export const createDiffPatcher = () =>
  new DiffPatcher({
    textDiff: {
      minLength: 120,
    },
  });
