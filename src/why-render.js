import { isValidElement, useEffect, useRef } from 'react';
import { isEqual, isFunction, isNaN } from 'lodash';
import { formatters } from 'jsondiffpatch';
import { createDiffPatcher } from './json-diff-patcher-factory';

const jsonDiffPatcher = createDiffPatcher();
/*
  OW-11072: deeply nested function props caused crash (Error: functions are not
  supported)
  This ticket explains workaround (custom plugin to erialise/deserialise
  functions into diffs https://github.com/benjamine/jsondiffpatch/issues/133
 */
jsonDiffPatcher.processor.pipes.diff.before('trivial', (context) => {
  if (
    typeof context.left === 'function' ||
    typeof context.right === 'function'
  ) {
    context.setResult([context.left, context.right]).exit();
  }
});

/*
  The jsondiffpatcher formatter uses JSON.stringify, which converts NaN and
  Infinity to null. This function just converts them to string representations
  first, so we don't lose detail when printing the output.
 */
export const replacer = (key, value) => {
  if (typeof value === 'function') {
    return 'function';
  }
  if (isNaN(value)) {
    return 'NaN';
  } else if (value === Infinity) {
    return 'Infinity';
  } else if (value === -Infinity) {
    return '-Infinity';
  } else {
    return value;
  }
};

export const explainDifferences = (previous, current) => {
  const delta = !(
    isFunction(previous) ||
    isValidElement(previous) ||
    isFunction(current) ||
    isValidElement(current)
  )
    ? jsonDiffPatcher.diff(previous, current)
    : null;
  if (delta === undefined && isEqual(previous, current)) {
    console.log(
      '%cNot shallow equal (same deep values but different reference)',
      'color: #FF5151',
    );
  } else if (delta === null) {
    console.log('%cFunction or component reference changed', 'color: #FF5151');
  } else if (delta) {
    const printableDelta = JSON.parse(JSON.stringify(delta, replacer));
    const prettyDiff = formatters.console.format(printableDelta);
    console.log(prettyDiff);
  }
};

export const whyRender = (
  previousProps,
  currentProps,
  previousState,
  currentState,
) => {
  Object.entries(currentProps).forEach(([key, value]) => {
    if (previousProps[key] !== value) {
      console.log(`%cProp ${key} changed`, 'color: #04D4F0');
      explainDifferences(previousProps[key], value);
    }
  });
  if (currentState) {
    Object.entries(currentState).forEach(([key, value]) => {
      if (previousState[key] !== value) {
        console.log(`%cState ${key} changed`, 'color: #059DC0');
        explainDifferences(previousState[key], value);
      }
    });
  }
};

export function useWhyRender(props) {
  const previousProps = useRef(props);

  useEffect(() => {
    Object.entries(props).forEach(([key, value]) => {
      if (previousProps.current[key] !== value) {
        console.log(`%cProp ${key} changed`, 'color: #04D4F0');
        explainDifferences(previousProps.current[key], value);
      }
    });
    previousProps.current = props;
  });
}
