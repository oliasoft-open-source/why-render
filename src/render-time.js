import { useEffect, useLayoutEffect, useRef } from 'react';

export const measureRenderTime = (instance) => {
  if (!instance) return;
  const endRenderTime = performance.now();
  const renderTime = endRenderTime - instance?.startRenderTime;
  console.log(
    `%cRender time: %c${renderTime.toFixed(3)} ms`,
    `color:#00A9E0`,
    `color:#FF5151`,
  );
};

export const useRenderTime = () => {
  const startRenderTime = useRef(null);
  const endRenderTime = useRef(null);

  useLayoutEffect(() => {
    startRenderTime.current = performance.now();
  });

  useEffect(() => {
    endRenderTime.current = performance.now();
    const timeDelta = endRenderTime.current - startRenderTime.current;
    console.log(
      `%cRender time: %c${timeDelta?.toFixed(3)} ms`,
      `color:#00A9E0`,
      `color:#FF5151`,
    );
  });
};
