import { explainDifferences, whyRender, useWhyRender } from './src/why-render';
import { useRenderTime } from './src/render-time';
import {
  withWhyRender,
  withRenderTime,
  withWhyRenderAndTime,
} from './src/hocs';

export {
  explainDifferences,
  whyRender,
  useWhyRender,
  useRenderTime,
  withWhyRender,
  withRenderTime,
  withWhyRenderAndTime,
};
