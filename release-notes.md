# WhyRender? Release Notes

## 1.1.0

- New `useRenderTime` hook
- New HOCs: `withWhyRender`, `withRenderTime` and `withWhyRenderWithTime`

## 1.0.7

- Upgraded to Node 22 [OW-18998](https://oliasoft.atlassian.net/browse/OW-18998)

## 1.0.6

- Fix mattermost url and changed channel name to Release bots [OW-14763](https://oliasoft.atlassian.net/browse/OW-14763)

## 1.0.5

- Fix release CI/CD pipeline ([OW-11691](https://oliasoft.atlassian.net/browse/OW-11691))

## 1.0.4

- fix handling of deeply nested function props ([OW-11072](https://oliasoft.atlassian.net/browse/OW-11072))

## 1.0.3

- Add initial unit tests

## 1.0.2

- Fix release pipeline

## 1.0.1

- Fix CI pipeline for publish release to MatterMost

## 1.0.0

- Initial release of `whyRender`, `useWhyRender`, and `explainDifferences` debug helpers
